var cluster = require("cluster");
var http = require("http");
require("dotenv").config();

// Master process code //
if (cluster.isMaster) {

    // Count the machine's CPUs //
    var cpuCount = require("os").cpus().length;

    // Create a worker for each CPU //
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    // Listen for terminating workers //
    cluster.on("exit", function (worker) {
        // Replace the terminated workers //
        console.log("Worker " + worker.id + " died");
        cluster.fork();
    });


// Worker process code //
} else {
    var AWS = require("aws-sdk");
    var express = require("express");
    var bodyParser = require("body-parser");
    var nodemailer = require("nodemailer");

    AWS.config.region = process.env.REGION

    // Environment settings for emails //
    var GMAIL_USER = process.env.GMAIL_USER;
    var GMAIL_PASS = process.env.GMAIL_PASS;

    // Start Express //
    var app = express();
    var router = express.Router();

    // Express configuration //
    app.use(bodyParser.json({limit: "10mb"}));
    app.use(bodyParser.urlencoded({limit: "10mb", extended: true}));
    app.set("views", __dirname + "/views");
    app.set("view engine", "ejs");
    var viewPath = __dirname + "/views/";
    app.use(express.static("static"));

    // POST route from contact form //
    app.post("/contact", function (req, res) {
  	  let mailOpts, smtpTrans;
  	  smtpTrans = nodemailer.createTransport({
    		host: "smtp.gmail.com",
    		port: 465,
    		secure: true,
    		auth: {
    		  user: GMAIL_USER,
    		  pass: GMAIL_PASS
    		}
  	  });

      // Email info //
  	  mailOpts = {
    		from: req.body.name + " &lt;" + req.body.email + "&gt;",
    		to: GMAIL_USER,
    		subject: "NEW MESSAGE on brankobajic.com",
    		text: `${req.body.name} (${req.body.email}) says: ${req.body.message}` // body.name/email/message come from the Contact form
  	  };

      // Send the message to my inbox //
  	  smtpTrans.sendMail(mailOpts, function (error, response) {
  	     if (error) {
  		   console.log("Mail send failure");
  		   }
  	  });
	});

	router.get("/",function(req,res){
		var sess=req.session;
		if(typeof sess !== "undefined"){
			var username=sess.username;
			res.render("index", { username:username});
		} else {
			res.render("index", {});
		}
	});

	app.use("/",router);
    var port = process.env.PORT || 3000;

    // Start the server //
    var server = app.listen(port, function () {
        console.log("Server running at http://127.0.0.1:" + port + "/");
    });
}
